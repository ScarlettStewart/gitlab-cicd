Makefile = a special file, containing shell commands that you create and name!

First I ran the below:

```
make test
```
Which then ran tests, at first I kept getting an error regarding but after doing some research I  found that upgrading the version of Pytest, snd including a version of `Werkzeug` to the requirements.txt helped a large amount 


![alt text](<Screenshot 2024-05-29 at 11.49.35.png>)

![alt text](<Screenshot 2024-05-29 at 11.40.10.png>)

Next, I had to choose a port, I wasn't sure if I had anything running on port 5000 so I chose port 5004.

```
export PORT=5004 
make run 
```
The terminal gave me an output like the below:

![alt text](<Screenshot 2024-05-29 at 12.03.18.png>)

Once going to the site, the below page was showed.
![alt text](<Screenshot 2024-05-29 at 11.52.39.png>)


Now that I have `started the application`, `seen it working` and `executed tests` it is now time to build the CI/CD pipeline.


The pipeline will be written in code and hosted inside the applications git repository via a YAML file titled `.gitlab-ci.yml`

I then decided as I wanted to keep pushing to git lab because of the documentation to instead fork the file and move over anything I wanted so that I could update the repo without disturbing the original files. When doing this I started running the commands from the beginning and decided to try port 5000 and found it to work.


